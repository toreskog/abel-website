
import yup from 'yup';

const steps = [
  {
    name: 'goal',
    schema: yup.object().shape({
      goal: yup.string().required(),
    }),
  },
  {
    name: 'gender',
    schema: yup.object().shape({
      gender: yup.string().required(),
    }),
  },
  {
    name: 'dateOfBirth',
    schema: yup.object().shape({
      dateOfBirth: yup.string().required(),
    }),
  },
  {
    name: 'height',
    schema: yup.object().shape({
      heightUnitOfMeasure: yup.string().required(),
      height: yup.string().required(),
    }),
  },
  {
    name: 'weight',
    schema: yup.object().shape({
      weightUnitOfMeasure: yup.string().required(),
      weight: yup.string().required(),
    }),
  },
  {
    name: 'activityLevel',
    schema: yup.object().shape({
      activityLevel: yup.string().required(),
    }),
    descriptions: {
      sedentary: {
        header: 'Sedentary',
        text: 'Little or no exercise',
      },
      lightlyActive: {
        header: 'Lightly active',
        text: 'Light exercise/ sports 1-3 days /week',
      },
      moderatelyActive: {
        header: 'Moderately active',
        text: 'Moderate exercise/ sports 6-7 days /week',
      },
      veryActive: {
        header: 'Very active',
        text: 'Hard exercise every day, or exercising 2 xs/day',
      },
      extraActive: {
        header: 'Extra active',
        text: 'Hard exercise 2 or more times per day, or training for marathon, or triathlon, etc.',
      },
    },
  },
  {
    name: 'device',
    schema: yup.object().shape({
      device: yup.string().required(),
    }),
  },
  {
    name: 'accountType',
    schema: yup.object().shape({
      accountType: yup.string().required(),
    }),
  },
  {
    name: 'form',
    schema: yup.object().shape({
      form: yup.string().required(),
    }),
  },
];

export { steps };
export default steps;
