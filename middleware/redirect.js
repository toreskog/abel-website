export default function ({ route, redirect }) {
  if (route.fullPath === '/signup') {
    redirect('/signup/goal');
  }
}
